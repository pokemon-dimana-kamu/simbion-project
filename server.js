const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');
const moment = require('moment')
const PORT = process.env.PORT || 8086
const app = express()
var session = require('express-session');
const pg = require('pg')
const format = require('string-format')

const client = new pg.Pool({
    user: 'db071',
    database: 'db071',
    // host: 'dbpg.cs.ui.ac.id',
    host: 'localhost',
    password: 'Oox8aH7k',
    port: 9012
});

app.use(session({
	secret: 'bangsdat',
	resave: true,
	saveUninitialized: true
}));
app.use(express.static('public'));
app.use(bodyParser.json()); // to support JSON bodies
app.use(bodyParser.urlencoded({ extended: true }));  // to support URL-encoded bodies
app.set('view engine', 'ejs')

var sess;
function isloggedin() {
	if(sess.username === undefined) {
		return false;
	} else {
		return true;
	}
}

app.get('/', function (req, res) {
	sess = req.session;
	if(isloggedin()) {
		if(sess.role === 'beasiswa') {
			res.render('homepage', {loggedin: true, ismahasiswa: true, isadmin: null, isdonatur: null});
		} else if(sess.role === 'admin') {
			res.render('homepage', {loggedin: true, ismahasiswa: null, isadmin: true, isdonatur: null});
		} else if(sess.role === 'donatur') {
			res.render('homepage', {loggedin: true, ismahasiswa: null, isadmin: null, isdonatur: true});
		} else {
			res.render('homepage', {loggedin: true, ismahasiswa: null, isadmin: null, isdonatur: null});
		}
	} else {
		res.render('homepage', {loggedin: null, ismahasiswa: null, isadmin: null, isdonatur: null});		}

})

app.post('/log', function(req, res) {
	res.redirect('/login');
})

app.post('/reg', function(req,res) {
  res.redirect('/register');
})

app.get('/login', function(req, res) {
	sess = req.session;
	//Session set when user Request our app via URL
	if(sess.username) {
	/*
	* This line check Session existence.
	* If it existed will do some action.
	*/
		res.render('/')
	}
	else {
		res.render('login', {error: null});
	}

})

app.post('/login', function(req, res) {
	client.connect()
	var uname = req.body.username;
	var upass = req.body.password;
	var obj;
	client.query(`select * from simbion.pengguna where simbion.pengguna.username='${uname}'`, (err, resp) => {
		obj = resp.rows[0];
	    if(obj === undefined) {
	    	console.log('tidak ditemukan user tersebut');
	    	res.render('login', {error: 'Error, Username tersebut tidak ditemukan'});
	    } else {
	    	var unameget = obj.username;
	    	var passget = obj.password;
	    	console.log('user founded');
	    	if(passget === upass){
	    		sess = req.session;
	    		req.session.role = obj.role;
	    		req.session.username = unameget;
				sess.username=req.body.username;
				sess.role = req.session.role;
				if(sess.role === 'beasiswa') {
	    			res.redirect('/')
	    		} else if(sess.role === 'admin') {
	    			res.redirect('/')
	    		} else if(sess.role === 'donatur') {
	    			res.redirect('/')
	    		} else {
	    			res.redirect('/')
	    		}
	    	} else {
	    		console.log('password salah');
	    		res.render('login', {error: 'Error, Password salah'});
	    	}
		}
    })

})



app.get('/register', function(req, res) {
	client.connect()
	res.render('register', {});
})

app.post('/register_mahasiswa', function(req, res) {
  res.redirect('/mahasiswa_registry')
})

app.post('/regstudent', function(req, res) {
	client.connect()
	var uname = req.body.username;
	var upass = req.body.password;
	var npm = req.body.npm;
	var email = req.body.email;
	var fullname = req.body.fullname;
	var telp = req.body.telp;
	var address = req.body.address;
	var domicile = req.body.domicile;
	var bank = req.body.bank;
	var accno = req.body.accno;
	var owner = req.body.owner;
	client.query(`select * from simbion.pengguna where simbion.pengguna.username='${uname}'`, (err, resp) => {
		obj = resp.rows[0];
	    if(obj === undefined) {
	    	console.log('username available');
	    	client.query(`select * from simbion.mahasiswa where simbion.mahasiswa.npm='${npm}'`, (err, respon) => {
	    		objek = respon.rows[0];
	    		if(objek === undefined) {
	    			console.log('mahasiswa belum ada di database');
	    			client.query('INSERT INTO SIMBION.PENGGUNA(username, password, role) values($1, $2, $3)',
	    				[uname, upass, 'beasiswa'])
	    			client.query('INSERT INTO SIMBION.MAHASISWA(npm, email, nama, no_telp, alamat_tinggal, alamat_domisili, nama_bank,'
	    				+ ' no_rekening, nama_pemilik, username) values($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)',
	    				[npm, email, fullname, telp, address, domicile, bank, accno, owner, uname]);
	    			sess = req.session;
		    		req.session.role = 'beasiswa';
		    		req.session.username = uname;
					sess.username= uname;
					sess.role = req.session.role;
	    			res.redirect('/');
	    		} else {
	    			console.log('mahasiswa sudah ada di database');
	    			res.render('register-mahasiswa', {error: 'Error, mahasiswa tersebut terdaftar.'});
	    		}
	    	})
	    } else {
			console.log('username unavailable');
			res.render('register-mahasiswa', {error: 'Error, sudah terdapat pengguna dengan username tersebut.'});
		}
    })
})

app.get('/mahasiswa_registry', function(req, res) {
	res.render('register-mahasiswa', {error: null});
})

app.post('/register_donatur', function(req, res) {
  res.redirect('/donatur_registry')
})

app.get('/donatur_registry', function(req, res) {
  res.render('register-donatur');
})

app.post('/donaturtype', function(req, res){
  if(req.body.donaturtype === "individual"){
    res.redirect('/register_donatur_individual')
  }
  if(req.body.donaturtype === "yayasan"){
    res.redirect('/register_donatur_yayasan')
  }
})

app.get('/register_donatur_individual', function(req, res){
	res.render('register-donatur-individual', {error: null});
})

app.get('/register_donatur_yayasan', function(req, res){
	res.render('register-donatur-yayasan', {error: null});
})

app.post('/individual_donation', function(req, res) {
	client.connect()
	var uname = req.body.username;
	var upass = req.body.password;
	var idnum = req.body.idnum;
	var nik = req.body.nik;
	var email = req.body.email;
	var fullname = req.body.fullname;
	var npwp = req.body.npwp;
	var telp = req.body.telp;
	var address = req.body.address;
	client.query(`select * from simbion.pengguna where simbion.pengguna.username='${uname}'`, (err, resp) => {
		obj = resp.rows[0];
	    if(obj === undefined) {
	    	console.log('username available');
	    	client.query(`select * from simbion.donatur where simbion.donatur.nomor_identitas='${nomor_identitas}'`, (err, respon) => {
	    		objek = respon.rows[0];
	    		if(objek === undefined) {
	    			console.log('donatur belum ada di database');
	    			client.query('INSERT INTO SIMBION.PENGGUNA(username, password, role) values($1, $2, $3)',
	    				[uname, upass, 'donatur'])
	    			client.query('INSERT INTO SIMBION.DONATUR(nomor_identitas, email, nama, npwp, no_telp, alamat, username)'
	    				+ ' values($1, $2, $3, $4, $5, $6, $7)',
	    				[nik, email, fullname, telp, npwp, address, uname]);
	    			sess = req.session;
		    		req.session.role = 'donatur';
		    		req.session.username = uname;
					sess.username= uname;
					sess.role = req.session.role;
	    			res.redirect('/');
	    		} else {
	    			console.log('donatur sudah ada di database');
	    			res.render('register-donatur-individual', {error: 'Error, Donatur tersebut sudah terdaftar.'})
	    		}
	    	})
	    } else {
			console.log('username unavailable');
			res.render('register-donatur-individual', {error: 'Error, sudah terdapat pengguna dengan username tersebut.'});
		}
    })
})

app.post('/yayasan_donation', function(req, res) {
	client.connect()
	var uname = req.body.username;
	var upass = req.body.password;
	var idnum = req.body.idnum;
	var sknum = req.body.sknum;
	var email = req.body.email;
	var name = req.body.nameyas;
	var telp = req.body.telp;
	var npwp = req.body.npwp;
	var address = req.body.address;
	client.query(`select * from simbion.pengguna where simbion.pengguna.username='${uname}'`, (err, resp) => {
		obj = resp.rows[0];
	    if(obj === undefined) {
	    	console.log('username available');
	    	client.query(`select * from simbion.donatur where simbion.donatur.nomor_identitas='${idnum}'`, (err, respon) => {
	    		objek = respon.rows[0];
	    		if(objek === undefined) {
	    			console.log('donatur belum ada di database');
	    			client.query(`select * from simbion.yayasan where simbion.yayasan.no_sk_yayasan='${sknum}'`, (err, response) => {
	    				objeckt = response.rows[0];
	    				if(objeckt === undefined) {
	    					console.log('yayasan belum ada di database');
	    					client.query('INSERT INTO SIMBION.PENGGUNA(username, password, role) values($1, $2, $3)',
			    				[uname, upass, 'donatur']);
			    			client.query('INSERT INTO SIMBION.DONATUR(nomor_identitas, email, nama, npwp, no_telp, alamat, username)'
			    				+ ' values($1, $2, $3, $4, $5, $6, $7)',
			    				[idnum, email, name, telp, npwp, address, uname], (err, results, fields) => {
			    					if(err) {
			    						return console.error(err.message);
			    					} else {
			    						client.query(`INSERT INTO SIMBION.YAYASAN(no_sk_yayasan, email, nama, no_telp_cp, nomor_identitas_donatur) values('${sknum}', '${email}', '${name}', '${telp}', '${idnum}')`, (salah, results, fields) => {
					    				if(err) {
					    					return console.error(salah.message);
			    						}
			    					});
			    					}
			    				});
			    			sess = req.session;
				    		req.session.role = 'donatur';
				    		req.session.username = uname;
							sess.username= uname;
							sess.role = req.session.role;
			    			res.redirect('/');
	    				} else {
	    					console.log('yayasan sudah ada di database');
							res.render('register-donatur-yayasan', {error: 'Error, yayasan tersebut sudah terdaftar.'});
	    				}
	    			})

	    		} else {
	    			console.log('donatur sudah ada di database');
					res.render('register-donatur-yayasan', {error: 'Error, donatur tersebut sudah terdaftar.'});
	    		}
	    	})
	    } else {
			console.log('username unavailable');
			res.render('register-donatur-yayasan', {error: 'Error, sudah terdapat pengguna dengan username tersebut.'});
		}
    })
})


app.post('/logout', function(req, res) {
req.session.destroy(function(err) {
  if(err) {
    console.log(err);
  } else {
    res.redirect('/');
  }
})
}
)

app.post('/interview_create', function(req, res) {
	res.redirect('/create_interview')
})

app.get('/create_interview', function(req, res) {
	if(isloggedin()) {
		if(req.session.role === 'admin') {
			res.render('tempat-wawancara', {error: null, notlogin : null});
		} else {
			res.render('tempat-wawancara', {error: 'Error, Anda bukan Admin', notlogin : true});
		}
	} else {
		res.render('tempat-wawancara', {error: 'Error, silahkan login terlebih dahulu', notlogin: true});
	}
})

app.post('/create_interview', function(req, res) {
	var code = req.body.kode;
	var nama = req.body.nama;
	var lokasi = req.body.lokasi;
	client.query(`select * from simbion.tempat_wawancara where simbion.tempat_wawancara.kode='${code}'`, (err, response) => {
	objeckt = response.rows[0];
	if(objeckt === undefined) {
		console.log('tempat wawancara belum terdaftar');
		client.query('INSERT INTO SIMBION.TEMPAT_WAWANCARA(kode, nama, lokasi) values($1, $2, $3)',
			    				[code, nama, lokasi]);
		res.redirect('/');
	} else {
		console.log('tempat tersebut sudah terdaftar');
		res.render('tempat-wawancara', {error: 'Tempat tersebut sudah terdaftar'})
	}
	})
})


app.post('/lihat_pengumuman', function(req, res) {
  res.redirect('/pengumuman/0')
})

app.get('/pilih_beasiswa', function(req, res) {
	res.render('pilih-beasiswa', {});
})

app.get('/form_daftar_beasiswa', function(req, res){
  render_beasiswa_daftar2(req, res, {
    kode: req.params.id
  });
})

app.post('/form_daftar_beasiswa', function(req, res) {
  console.log(req.body)
  var kode_beasiswa = req.body.kode_beasiswa
  var NPM = req.body.NPM
  var Email = req.body.Email
  var IPS = req.body.IPS

  var nomorUrut = 1
  var waktu_daftar = '2017-03-01 00:00:00'
  var status_daftar = 'terdaftar'
  var status_terima = 'ditolak'

  var insertSkema = 'insert into simbion.pendaftaran ' +
                      'values ($1, $2, $3, $4, $5, $6)'

    client.query(insertSkema, [nomorUrut, kode_beasiswa, NPM, waktu_daftar, status_daftar, status_terima], (error, result) => {
      if (error) {
      	console.log(error);
        res.status(500)
        res.render('internal-error', {})
        return
      } else {
      	res.redirect('/');
      }})
 //res.render('5_form_daftar_beasiswa');
})

function render_beasiswa_daftar(req, res, data) {
  var queryStr = `select kode from simbion.skema_beasiswa order by kode asc`

  //var queryStr2 = 'select no_urut from simbion.skema_beasiswa_aktif order by kode asc'
  client.connect()
  client.query(queryStr, (error, result) => {
    if (error) {
      res.status(500)
      res.render('internal-error', {})
      return
    }
    res.render('5_form_daftar_beasiswa_selected', {
      data: data,
      kode: result.rows
    });
  })

}

function render_beasiswa_daftar2(req, res, data) {
  var queryStr = 'select kode from simbion.skema_beasiswa order by kode asc'

  client.connect()
  client.query(queryStr, (error, result) => {
    if (error) {
      res.status(500)
      res.render('internal-error', {})
      return
    }

    res.render('5_form_daftar_beasiswa', {
      data: data,
      kode: result.rows
    });
  })
}

function render_pembayaran_beasiswa(req, res, data) {
  var queryStr = 'select kode from simbion.skema_beasiswa order by kode asc'

  client.connect()
  client.query(queryStr, (error, result) => {
    if (error) {
      res.status(500)
      res.render('internal-error', {})
      return
    }

    res.render('9_pembayaran_beasiswa', {
      data: data,
      kode: result.rows
    });
  })
}

app.get('/form_daftar_beasiswa_selected/:id', function(req, res){
  render_beasiswa_daftar(req, res, {
    id: req.params.id
  });
})

app.post('/form_daftar_beasiswa_selected', function(req, res){
  console.log(req.body)
  var kode_beasiswa = req.body.kode_beasiswa
  var NPM = req.body.NPM
  var Email = req.body.Email
  var IPS = req.body.IPS

  var nomorUrut = 1
  var waktu_daftar = '2017-03-01 00:00:00'
  var status_daftar = 'terdaftar'
  var status_terima = 'ditolak'

  var insertSkema = 'insert into simbion.pendaftaran' +
                      'values ($1, $2, $3, $4, $5, $6)'

    client.query(insertSkema, [nomorUrut, kode_beasiswa, NPM, waktu_daftar, status_daftar, status_terima], (error, result) => {
      if (error) {
        res.status(500)
        res.render('internal-error', {})
        return
      }})
 //res.render('5_form_daftar_beasiswa');
})

app.get('/pilih_calon', function(req, res) {
 res.render('6b', {});
})

app.get('/pembayaran_beasiswa', function(req, res) {
 render_pembayaran_beasiswa(req, res, {
    kode: req.params.id
  });
})

app.post('/pembayaran_beasiswa', function(req, res) {
  var nomorUrut = req.body.nomorUrut
  var nominal = req.body.nominal
  var tanggalBayar = req.body.tanggalBayar
  var keterangan = req.body.keterangan
  var NPM = req.body.NPM
  var noUSB = req.body.noUSB
  var kode = req.body.kode

  var insertSkema = 'insert into simbion.pendaftaran' +
                      'values ($1, $2, $3, $4, $5, $6, $7)'

    client.query(insertSkema, [nomorUrut, kode, noUSB, NPM, keterangan, tanggalBayar, nominal], (error, result) => {
      if (error) {
        res.status(500)
        res.render('internal-error', {})
        return
      }})
})

app.get('/pilih_calon2', function(req, res) {
 res.render('6a', {});
})


function render_paket_beasiswa(req, res, data) {
  var queryStr = 'select distinct jenis from simbion.skema_beasiswa order by jenis'

  client.connect()
  client.query(queryStr, (error, result) => {
    if (error) {
      res.status(500)
      res.render('internal-error', {})
      return
    }

    res.render('beasiswa-add-paket', {
      data: data,
      rowsJenis: result.rows
    });
  })
}


// DONE
app.get('/beasiswa/add-paket', function(req, res) {
  render_paket_beasiswa(req, res, {})
})


// DONE
app.post('/beasiswa/add-paket', function(req, res) {
  var kode = req.body.kode
  var nama_paket = req.body.nama_paket
  var jenis_paket = req.body.jenis_paket
  var deskripsi = req.body.deskripsi
  var syarat = req.body.syarat

  if (kode == undefined || nama_paket == undefined || jenis_paket == undefined
      || deskripsi == undefined || syarat == undefined){

    render_paket_beasiswa(req, res, {
      status: 'error',
      info: 'Error',
      message: 'Field tidak boleh kosong!'
    })

    return
  }

  var queryStr = 'select * from simbion.skema_beasiswa where kode = $1'

  client.connect()
  client.query(queryStr, [kode], (error, result) => {
    if (error) {
      res.status(500)
      res.render('internal-error', {})
      return
    }

    if (result.rows.length > 0) {

      render_paket_beasiswa(req, res, {
        status: 'error',
        info: 'Error',
        message: 'Kode sudah digunakan!'
      })

      return
    }

    // TODO, nomor identitas donatur 52936384
    var nomorId = 52936384

    var insertSkema = 'insert into simbion.skema_beasiswa ' +
                      'values ($1, $2, $3, $4, $5)'

    client.query(insertSkema, [kode, nama_paket, jenis_paket, deskripsi, nomorId], (error, result) => {
      if (error) {
        res.status(500)
        res.render('internal-error', {})
        return
      }

      var insertSyarat = "insert into simbion.syarat_beasiswa values "
      for (var i = 0; i < syarat.length; i++) {
        if (i > 0) insertSyarat += ', '
        insertSyarat += "($1, $" + (i+2) + ")"
      }

      syarat.unshift(kode) // add kode at the beginning

      client.query(insertSyarat, syarat, (error, result) => {
          if (error) {
            res.status(500)
            res.render('internal-error', {})
            return
          }

          render_paket_beasiswa(req, res, {
            status: 'success',
            info: 'Success',
            message: 'Paket beasiswa berhasil dibuat!'
        })
      })
    })
  })
})


function render_beasiswa(req, res, data) {
  var queryStr = 'select kode from simbion.skema_beasiswa order by kode asc'

  client.connect()
  client.query(queryStr, (error, result) => {
    if (error) {
      res.status(500)
      res.render('internal-error', {})
      return
    }

    res.render('beasiswa-add', {
      data: data,
      rowsKode: result.rows
    });
  })
}


app.get('/beasiswa/add', function(req, res) {
  render_beasiswa(req, res, {});
})


app.post('/beasiswa/add', function(req, res) {
  var kode = req.body.kode
  var noUrut = req.body.noUrut
  var tgl_mulai = req.body.tgl_mulai
  var tgl_tutup = req.body.tgl_tutup

  if (kode == undefined || noUrut == undefined || tgl_mulai == undefined
      || tgl_tutup == undefined){

    render_beasiswa(req, res, {
      status: 'error',
      info: 'Error',
      message: 'Field tidak boleh kosong!'
    })

    return
  }

  if (Date.parse(tgl_mulai) > Date.parse(tgl_tutup)) {
    render_beasiswa(req, res, {
      status: 'error',
      info: 'Error',
      message: 'Tanggal mulai harus lebih dahulu!'
    })

    return
  }

  if (Date.parse(new Date()) > Date.parse(tgl_tutup)) {
    render_beasiswa(req, res, {
      status: 'error',
      info: 'Error',
      message: 'Tanggal sekarang harus lebih dahulu!'
    })

    return
  }

  var queryStr = 'select * from simbion.skema_beasiswa_aktif where ' +
                'kode_skema_beasiswa = $1 or no_urut = $2'

  client.connect()
  client.query(queryStr, [kode, noUrut], (error, result) => {
    if (error) {
      res.status(500)
      res.render('internal-error', {})
      return
    }

    if (result.rows.length > 0) {

      render_beasiswa(req, res, {
        status: 'error',
        info: 'Error',
        message: 'Kode atau nomor urut sudah digunakan!'
      })

      return
    }

    var insertBeasiswa = "insert into simbion.skema_beasiswa_aktif " +
                        "values ($1, $2, $3, $4, $5, 'Aktif', 0, 0)"

    var periode = moment(tgl_mulai).format("DD/MM/YYYY").toString() +
                  '-' + moment(tgl_tutup).format("DD/MM/YYYY").toString()

    client.query(insertBeasiswa, [kode, noUrut, tgl_mulai, tgl_tutup, periode], (error, result) => {
       if (error) {
        res.status(500)
        res.render('internal-error', {})
        return
      }

      render_beasiswa(req, res, {
        status: 'success',
        info: 'Success',
        message: 'Beasiswa berhasil ditambah!'
      })

    })
  })

})


// DONE
app.get('/beasiswa/:page', function(req, res) {
  var page = req.params.page

  if (page < 1 || isNaN(page)) {
    res.redirect('/beasiswa/1')
    return
  }

  var queryStr = 'select SB.kode, SB.nama, SBA.tgl_tutup_pendaftaran, ' +
            'SBA.status, SBA.jumlah_pendaftar ' +
            'from simbion.skema_beasiswa SB, ' +
            'simbion.skema_beasiswa_aktif SBA ' +
            'where SB.kode = SBA.kode_skema_beasiswa ' +
            'order by SBA.tgl_tutup_pendaftaran desc limit 10 ' +
            'offset $1'

  client.connect()
  client.query(queryStr, [((page-1)*10).toString()], (error, result) => {
    if (error) {
      res.status(500)
      res.render('internal-error', {})
      return
    }

    res.render('beasiswa', {
      moment: moment,
      page: page,
      rows: result.rows });
  })
})


// DONE
app.get('/beasiswa/detail/:code', function(req, res) {
  var code = req.params.code

  if (isNaN(code)) {
    res.status(404)
    res.render('not-found', {})
    return
  }

  var querySyrt = 'select * from simbion.syarat_beasiswa ' +
                'where kode_beasiswa = $1'

  client.connect()
  client.query(querySyrt, [code.toString()], (error, result) => {
    var syaratRes = []

    if (error) {
      res.status(500)
      res.render('internal-error', {})
      return
    }

    syaratRes = result.rows

    var queryStr = 'select * from simbion.skema_beasiswa SB ' +
                'where kode = $1'

    client.query(queryStr, [code.toString()], (error, result) => {
      if (error || result.rows.length > 1) {
        res.status(500)
        res.render('internal-error', {})
        return
      }
      if (result.rows.length == 0) {
        res.status(404)
        res.render('not-found', {})
        return
      }

      if (result.rows.length == 1) {
        res.render('beasiswa-detail', {
          data: result.rows[0],
          dataSyarat: syaratRes
        });
      }
    })
  })
})


// DONE
app.get('/pengumuman/:page', function(req, res, next) {
  var page = req.params.page

  if (isNaN(page)) {
    next()
    return
  }

  if (page < 1) {
    res.redirect('/pengumuman/1')
    return
  }

  var queryStr = 'select * from simbion.pengumuman order by ' +
            'tanggal desc limit 10 ' +
            'offset $1'

  client.connect()
  client.query(queryStr, [((page-1)*10).toString()], (error, result) => {
    if (error) {
      res.status(500)
      res.render('internal-error', {})
      return
    }

    res.render('pengumuman', {
      moment: moment,
      page: page,
      rows: result.rows });
  })
})


// DONE
app.get('/pengumuman/detail/', function(req, res){
  var tanggal = req.query.tanggal
  var kode = req.query.kode
  var noUrut = req.query.noUrut
  var username = req.query.username

  if (tanggal == undefined || kode == undefined
      || noUrut == undefined || username == undefined){

    res.status(404)
    res.render('not-found', {})
    return
  }

  var queryStr = 'select * from simbion.pengumuman ' +
                'where tanggal = $1 and ' +
                'kode_skema_beasiswa = $2 and ' +
                'no_urut_skema_beasiswa_aktif = $3 and ' +
                'username = $4'

  client.connect()
  client.query(queryStr, [tanggal.toString(), kode.toString(),
          noUrut.toString(), username.toString()], (error, result) => {

    if (error || result.rows.length > 1) {
      res.status(500)
      res.render('internal-error', {})
      return
    }
    if (result.rows.length == 0) {
      res.status(404)
      res.render('not-found', {})
      return
    }

    if (result.rows.length == 1) {
      res.render('pengumuman-detail', {
        moment: moment,
        data: result.rows[0],
      });
    }
  })
})


app.listen(PORT, function () {
  console.log(`Example app listening on port ${PORT}!`)
})